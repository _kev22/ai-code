
#include <stdio.h>
#define MAX 100



struct VL {
    char desde[20];
    char hacia[20];
    int distancia;
    char visitado;
};

struct VL vuelos[MAX];  /* array de estructuras de la bd */
int pos_ult = 0;        /* numero de entradas en la bd vuelos */
int encuentra_pos = 0;  /* indice de busqueda en la bd vuelos */
int cabeza_pila = 0;    /* cabeza de la pila */

struct pila {
    char desde[20];
    char hacia[20];
    int dist;
};

struct pila pila_rt[MAX];   /* pila de vueltas atras */

main() {
    char desde[20], hacia[20];

    inicia();
    printf("Desde? = ");
    gets(desde);
    printf("Hacia? = ");
    gets(hacia);
    hay_vuelo(desde, hacia);
    ruta(hacia);
}

inicia() {
    declara_vuelo("Panama", "Cocle", 1000);
    declara_vuelo("Cocle", "Veraguas", 1000);
    declara_vuelo("Panama", "Colon", 800);
    declara_vuelo("Panama", "Veraguas", 1900); 
    declara_vuelo("Colon", "Bocas Del Toro", 1500);
    declara_vuelo("Colon", "Chiriqui", 1800);
    declara_vuelo("Colon", "Cocle", 500);
    declara_vuelo("Veraguas", "Herrera", 1000);
    declara_vuelo("Veraguas", "Los Santos", 1500);
    declara_vuelo("Los Santos", "Chiriqui", 1500);
    declara_vuelo("Veraguas", "Chiriqui", 1000);
}

/* Colocar datos dentro de la bd vuelos */
declara_vuelo(desde, hacia, dist)
char *desde, *hacia;
int dist;
{
    if (pos_ult < MAX) {
        strcpy(vuelos[pos_ult].desde, desde);
        strcpy(vuelos[pos_ult].hacia, hacia);
        vuelos[pos_ult].distancia = dist;
        vuelos[pos_ult].visitado = 0;
        pos_ult++;
    }
    else
        printf("Base de datos vuelos llena. \n");
}
    
/* muestra la ruta y la distancia */
ruta(hacia)
char *hacia;
{
    int dist, t;
    dist = 0;
    t = 0;
    while (t < cabeza_pila) {
        printf("%s hacia ", pila_rt[t].desde);
        dist += pila_rt[t].dist;
        t++;
    }
    printf("%s\n", hacia);
    printf("La distancia es: %d\n", dist);
}

/* Si hay conexion entre desde y hacia, entonces devuelve la distancia
 * del vuelo. Si no, devuelve 0 */
unidas(desde, hacia)
char *desde, *hacia;
{
    register int t;
    for (t = pos_ult -1; t > -1; t--) {
        if (!strcmp(vuelos[t].desde, desde) &&
                !strcmp(vuelos[t].hacia, hacia))
            return vuelos[t].distancia;
    }
    return 0; /* no encontrado */
}

/* dado un desde, encuentra cualquier_lugar */
encuentra(desde, cualquier_lugar)
char *desde, *cualquier_lugar;
{
    encuentra_pos = 0;
    while (encuentra_pos < pos_ult) {
        if(!strcmp(vuelos[encuentra_pos].desde, desde) &&
                !vuelos[encuentra_pos].visitado) {
            strcpy(cualquier_lugar, vuelos[encuentra_pos].hacia);
            vuelos[encuentra_pos].visitado = 1;
            return vuelos[encuentra_pos].distancia;
        }
        encuentra_pos++;
    }
    return 0;
}


/* Determina Si Hay una Ruta Entre de Desde y Hacia */ 
hay_vuelo(desde,hacia)
char *desde, *hacia; {
 int d, dist; 
 char cualquier_lugar[20];
 if(d=unidas(desde,hacia)) 
 { 
 mete_pila(desde, hacia, d);
 return; 
 }
 
if(dist=encuentra(desde,cualquier_lugar))
{
	mete_pila(desde,hacia,dist);
	hay_vuelo(cualquier_lugar , hacia );}
	else 
	if(cabeza_pila>0)
	{ 
	 saca_pila(desde,hacia,dist);
	hay_vuelo(desde,hacia);}
 }

/*Rutina De Pila */

mete_pila(desde,hacia,dist)
char *hacia, *desde; 
int dist;
{
	if(cabeza_pila<MAX )
	{
		strcpy(pila_rt[cabeza_pila].desde,desde);
		strcpy(pila_rt[cabeza_pila].hacia,hacia);
		pila_rt[cabeza_pila].dist =dist; 
		cabeza_pila++;}
		
		else printf("Pila llena.\n");} 
	
saca_pila(desde,hacia,dist)
char *desde, *hacia;
int *dist;
{
	if (cabeza_pila>0)
	{
		cabeza_pila--;
		strcpy(desde,pila_rt[cabeza_pila].desde);
		strcpy(desde,pila_rt[cabeza_pila].hacia);
		*dist=pila_rt[cabeza_pila].dist;
	}
	else printf("Pila vacia. \n"); 	
}




